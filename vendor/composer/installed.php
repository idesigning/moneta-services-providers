<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'idesigning/moneta-services-providers',
        'dev' => false,
    ),
    'versions' => array(
        'idesigning/moneta-services-providers' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'wsdltophp/packagebase' => array(
            'pretty_version' => '5.0.2',
            'version' => '5.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wsdltophp/packagebase',
            'aliases' => array(),
            'reference' => '151ffb8dfc723204bdde9a16017c1d584fb9f0de',
            'dev_requirement' => false,
        ),
    ),
);
