<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://www.moneta.ru/services-providers.wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://www.moneta.ru/services-providers.wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \MonetaServiceProviders\ClassMap::get(),
];
/**
 * Samples for Get ServiceType
 */
$get = new \MonetaServiceProviders\ServiceType\Get($options);
/**
 * Sample call for GetNextStep operation/method
 */
if ($get->GetNextStep(new \MonetaServiceProviders\StructType\GetNextStepRequest()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetServiceProviders operation/method
 */
if ($get->GetServiceProviders(new \MonetaServiceProviders\StructType\GetServiceProvidersRequest()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Find ServiceType
 */
$find = new \MonetaServiceProviders\ServiceType\Find($options);
/**
 * Sample call for FindServiceProviderById operation/method
 */
if ($find->FindServiceProviderById(new \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest()) !== false) {
    print_r($find->getResult());
} else {
    print_r($find->getLastError());
}
/**
 * Samples for Async ServiceType
 */
$async = new \MonetaServiceProviders\ServiceType\Async($options);
/**
 * Sample call for Async operation/method
 */
if ($async->Async(new \MonetaServiceProviders\StructType\AsyncRequest()) !== false) {
    print_r($async->getResult());
} else {
    print_r($async->getLastError());
}
