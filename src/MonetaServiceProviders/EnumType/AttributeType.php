<?php

declare(strict_types=1);

namespace MonetaServiceProviders\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AttributeType EnumType
 * Meta information extracted from the WSDL
 * - documentation: Возможные типы полей INTEGER - числовое TEXT - текстовое ENUM - перечень значений DATE - дата DATALIST - текстовое поле с возможностью выбор из
 * заранее определенных значений NOTE - примечание (текст), в значении (value) лежит текст который необходимо показывать пользователю. CARDNUMBER -
 * номер банковской карты. INTERNAL - внутреннее поле, его значение вычисляется в процессе проведения операции или оно постоянно.
 * @subpackage Enumerations
 */
class AttributeType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MASKED'
     * @return string 'MASKED'
     */
    const VALUE_MASKED = 'MASKED';
    /**
     * Constant for value 'INTEGER'
     * @return string 'INTEGER'
     */
    const VALUE_INTEGER = 'INTEGER';
    /**
     * Constant for value 'TEXT'
     * @return string 'TEXT'
     */
    const VALUE_TEXT = 'TEXT';
    /**
     * Constant for value 'ENUM'
     * @return string 'ENUM'
     */
    const VALUE_ENUM = 'ENUM';
    /**
     * Constant for value 'DATE'
     * @return string 'DATE'
     */
    const VALUE_DATE = 'DATE';
    /**
     * Constant for value 'DATALIST'
     * @return string 'DATALIST'
     */
    const VALUE_DATALIST = 'DATALIST';
    /**
     * Constant for value 'NOTE'
     * @return string 'NOTE'
     */
    const VALUE_NOTE = 'NOTE';
    /**
     * Constant for value 'CARDNUMBER'
     * @return string 'CARDNUMBER'
     */
    const VALUE_CARDNUMBER = 'CARDNUMBER';
    /**
     * Constant for value 'INTERNAL'
     * @return string 'INTERNAL'
     */
    const VALUE_INTERNAL = 'INTERNAL';
    /**
     * Return allowed values
     * @uses self::VALUE_MASKED
     * @uses self::VALUE_INTEGER
     * @uses self::VALUE_TEXT
     * @uses self::VALUE_ENUM
     * @uses self::VALUE_DATE
     * @uses self::VALUE_DATALIST
     * @uses self::VALUE_NOTE
     * @uses self::VALUE_CARDNUMBER
     * @uses self::VALUE_INTERNAL
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MASKED,
            self::VALUE_INTEGER,
            self::VALUE_TEXT,
            self::VALUE_ENUM,
            self::VALUE_DATE,
            self::VALUE_DATALIST,
            self::VALUE_NOTE,
            self::VALUE_CARDNUMBER,
            self::VALUE_INTERNAL,
        ];
    }
}
