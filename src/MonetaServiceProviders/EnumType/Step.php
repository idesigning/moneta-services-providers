<?php

declare(strict_types=1);

namespace MonetaServiceProviders\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Step EnumType
 * Meta information extracted from the WSDL
 * - documentation: Возможные шаги при оплате провайдера
 * @subpackage Enumerations
 */
class Step extends AbstractStructEnumBase
{
    /**
     * Constant for value 'PRE'
     * @return string 'PRE'
     */
    const VALUE_PRE = 'PRE';
    /**
     * Constant for value 'PAY'
     * @return string 'PAY'
     */
    const VALUE_PAY = 'PAY';
    /**
     * Return allowed values
     * @uses self::VALUE_PRE
     * @uses self::VALUE_PAY
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_PRE,
            self::VALUE_PAY,
        ];
    }
}
