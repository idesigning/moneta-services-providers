<?php

declare(strict_types=1);

namespace MonetaServiceProviders\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Version EnumType
 * Meta information extracted from the WSDL
 * - documentation: The version enumeration which is used in ServiceProviders. If functionality depends on certain version it will be described for each request type. VERSION_1 is considered by default. | Список версий, который
 * используется в ServiceProviders. Если функциональность зависит от конкретной версии, то в описании запроса это будет указано дополнительно.
 * Если версия не указана, то по умолчанию используется VERSION_1.
 * @subpackage Enumerations
 */
class Version extends AbstractStructEnumBase
{
    /**
     * Constant for value 'VERSION_1'
     * @return string 'VERSION_1'
     */
    const VALUE_VERSION_1 = 'VERSION_1';
    /**
     * Constant for value 'VERSION_2'
     * @return string 'VERSION_2'
     */
    const VALUE_VERSION_2 = 'VERSION_2';
    /**
     * Constant for value 'VERSION_3'
     * @return string 'VERSION_3'
     */
    const VALUE_VERSION_3 = 'VERSION_3';
    /**
     * Return allowed values
     * @uses self::VALUE_VERSION_1
     * @uses self::VALUE_VERSION_2
     * @uses self::VALUE_VERSION_3
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_VERSION_1,
            self::VALUE_VERSION_2,
            self::VALUE_VERSION_3,
        ];
    }
}
