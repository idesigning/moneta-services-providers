<?php

declare(strict_types=1);

namespace MonetaServiceProviders\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AsyncStatus EnumType
 * Meta information extracted from the WSDL
 * - documentation: The status of a asynchronous task in MONETA.RU. | Тип, описывающий статусы асинхронных запросов в системе MONETA.RU. Данный тип может иметь только
 * определенные значения, описанные ниже.
 * @subpackage Enumerations
 */
class AsyncStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'INPROGRESS'
     * Meta information extracted from the WSDL
     * - documentation: Асихронная задача в обработке. | MONETA.RU is processing the asynchronous task.
     * @return string 'INPROGRESS'
     */
    const VALUE_INPROGRESS = 'INPROGRESS';
    /**
     * Constant for value 'CREATED'
     * Meta information extracted from the WSDL
     * - documentation: Асихронная задача создана. | Asynchronous task is registered in MONETA.RU.
     * @return string 'CREATED'
     */
    const VALUE_CREATED = 'CREATED';
    /**
     * Return allowed values
     * @uses self::VALUE_INPROGRESS
     * @uses self::VALUE_CREATED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_INPROGRESS,
            self::VALUE_CREATED,
        ];
    }
}
