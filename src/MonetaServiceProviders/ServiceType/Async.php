<?php

declare(strict_types=1);

namespace MonetaServiceProviders\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Async ServiceType
 * @subpackage Services
 */
class Async extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Async
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \MonetaServiceProviders\StructType\AsyncRequest $asyncRequest
     * @return \MonetaServiceProviders\StructType\AsyncResponse|bool
     */
    public function Async(\MonetaServiceProviders\StructType\AsyncRequest $asyncRequest)
    {
        try {
            $this->setResult($resultAsync = $this->getSoapClient()->__soapCall('Async', [
                $asyncRequest,
            ], [], $this->inputHeaders, $this->outputHeaders));
        
            return $resultAsync;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
