<?php

declare(strict_types=1);

namespace MonetaServiceProviders\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Find ServiceType
 * @subpackage Services
 */
class Find extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named FindServiceProviderById
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse|bool
     */
    public function FindServiceProviderById(\MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest)
    {
        try {
            $this->setResult($resultFindServiceProviderById = $this->getSoapClient()->__soapCall('FindServiceProviderById', [
                $findServiceProviderByIdRequest,
            ], [], $this->inputHeaders, $this->outputHeaders));
        
            return $resultFindServiceProviderById;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
