<?php

declare(strict_types=1);

namespace MonetaServiceProviders\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetNextStep
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest
     * @return \MonetaServiceProviders\StructType\GetNextStepResponse|bool
     */
    public function GetNextStep(\MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest)
    {
        try {
            $this->setResult($resultGetNextStep = $this->getSoapClient()->__soapCall('GetNextStep', [
                $getNextStepRequest,
            ], [], $this->inputHeaders, $this->outputHeaders));
        
            return $resultGetNextStep;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetServiceProviders
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \MonetaServiceProviders\StructType\GetServiceProvidersRequest $getServiceProvidersRequest
     * @return \MonetaServiceProviders\StructType\GetServiceProvidersResponse|bool
     */
    public function GetServiceProviders(\MonetaServiceProviders\StructType\GetServiceProvidersRequest $getServiceProvidersRequest)
    {
        try {
            $this->setResult($resultGetServiceProviders = $this->getSoapClient()->__soapCall('GetServiceProviders', [
                $getServiceProvidersRequest,
            ], [], $this->inputHeaders, $this->outputHeaders));
        
            return $resultGetServiceProviders;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \MonetaServiceProviders\StructType\GetNextStepResponse|\MonetaServiceProviders\StructType\GetServiceProvidersResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
