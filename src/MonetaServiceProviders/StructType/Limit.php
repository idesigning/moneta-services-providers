<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for limit StructType
 * @subpackage Structs
 */
class Limit extends AbstractStructBase
{
    /**
     * The min
     * Meta information extracted from the WSDL
     * - documentation: Минимальная сумма при проведении операции
     * @var float|null
     */
    protected ?float $min = null;
    /**
     * The max
     * Meta information extracted from the WSDL
     * - documentation: Максимальная сумма при проведении операции
     * @var float|null
     */
    protected ?float $max = null;
    /**
     * The fix
     * Meta information extracted from the WSDL
     * - documentation: Фиксированная сумма, если для данного провайдера сумма операции строго определена.
     * @var float|null
     */
    protected ?float $fix = null;
    /**
     * Constructor method for limit
     * @uses Limit::setMin()
     * @uses Limit::setMax()
     * @uses Limit::setFix()
     * @param float $min
     * @param float $max
     * @param float $fix
     */
    public function __construct(?float $min = null, ?float $max = null, ?float $fix = null)
    {
        $this
            ->setMin($min)
            ->setMax($max)
            ->setFix($fix);
    }
    /**
     * Get min value
     * @return float|null
     */
    public function getMin(): ?float
    {
        return $this->min;
    }
    /**
     * Set min value
     * @param float $min
     * @return \MonetaServiceProviders\StructType\Limit
     */
    public function setMin(?float $min = null): self
    {
        // validation for constraint: float
        if (!is_null($min) && !(is_float($min) || is_numeric($min))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($min, true), gettype($min)), __LINE__);
        }
        $this->min = $min;
        
        return $this;
    }
    /**
     * Get max value
     * @return float|null
     */
    public function getMax(): ?float
    {
        return $this->max;
    }
    /**
     * Set max value
     * @param float $max
     * @return \MonetaServiceProviders\StructType\Limit
     */
    public function setMax(?float $max = null): self
    {
        // validation for constraint: float
        if (!is_null($max) && !(is_float($max) || is_numeric($max))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($max, true), gettype($max)), __LINE__);
        }
        $this->max = $max;
        
        return $this;
    }
    /**
     * Get fix value
     * @return float|null
     */
    public function getFix(): ?float
    {
        return $this->fix;
    }
    /**
     * Set fix value
     * @param float $fix
     * @return \MonetaServiceProviders\StructType\Limit
     */
    public function setFix(?float $fix = null): self
    {
        // validation for constraint: float
        if (!is_null($fix) && !(is_float($fix) || is_numeric($fix))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($fix, true), gettype($fix)), __LINE__);
        }
        $this->fix = $fix;
        
        return $this;
    }
}
