<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Fieldsets StructType
 * Meta information extracted from the WSDL
 * - documentation: Визуальная группировка полей
 * @subpackage Structs
 */
class Fieldsets extends AbstractStructBase
{
    /**
     * The fieldset
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Fieldset[]
     */
    protected ?array $fieldset = null;
    /**
     * Constructor method for Fieldsets
     * @uses Fieldsets::setFieldset()
     * @param \MonetaServiceProviders\StructType\Fieldset[] $fieldset
     */
    public function __construct(?array $fieldset = null)
    {
        $this
            ->setFieldset($fieldset);
    }
    /**
     * Get fieldset value
     * @return \MonetaServiceProviders\StructType\Fieldset[]
     */
    public function getFieldset(): ?array
    {
        return $this->fieldset;
    }
    /**
     * This method is responsible for validating the values passed to the setFieldset method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFieldset method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFieldsetForArrayConstraintsFromSetFieldset(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fieldsetsFieldsetItem) {
            // validation for constraint: itemType
            if (!$fieldsetsFieldsetItem instanceof \MonetaServiceProviders\StructType\Fieldset) {
                $invalidValues[] = is_object($fieldsetsFieldsetItem) ? get_class($fieldsetsFieldsetItem) : sprintf('%s(%s)', gettype($fieldsetsFieldsetItem), var_export($fieldsetsFieldsetItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The fieldset property can only contain items of type \MonetaServiceProviders\StructType\Fieldset, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set fieldset value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Fieldset[] $fieldset
     * @return \MonetaServiceProviders\StructType\Fieldsets
     */
    public function setFieldset(?array $fieldset = null): self
    {
        // validation for constraint: array
        if ('' !== ($fieldsetArrayErrorMessage = self::validateFieldsetForArrayConstraintsFromSetFieldset($fieldset))) {
            throw new InvalidArgumentException($fieldsetArrayErrorMessage, __LINE__);
        }
        $this->fieldset = $fieldset;
        
        return $this;
    }
    /**
     * Add item to fieldset value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Fieldset $item
     * @return \MonetaServiceProviders\StructType\Fieldsets
     */
    public function addToFieldset(\MonetaServiceProviders\StructType\Fieldset $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\Fieldset) {
            throw new InvalidArgumentException(sprintf('The fieldset property can only contain items of type \MonetaServiceProviders\StructType\Fieldset, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->fieldset[] = $item;
        
        return $this;
    }
}
