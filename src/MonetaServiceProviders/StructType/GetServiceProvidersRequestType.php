<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetServiceProvidersRequestType StructType
 * @subpackage Structs
 */
class GetServiceProvidersRequestType extends Entity
{
    /**
     * The targetAccountId
     * Meta information extracted from the WSDL
     * - documentation: Correspondent account number in MONETA.RU | Корреспонденский номер счета в системе MONETA.RU | Тип, описывающий номер счета в системе MONETA.RU. Счет
     * задается числом, минимальное значение которого равно 1, а максимальное состоит из 10 цифр. | Account type in MONETA.RU. Must contain up to 10 digits, minimum value is 1.
     * - base: xsd:long
     * - minInclusive: 1
     * - minOccurs: 0
     * - totalDigits: 10
     * @var int|null
     */
    protected ?int $targetAccountId = null;
    /**
     * The searchString
     * Meta information extracted from the WSDL
     * - documentation: Search string. Search provider by entering this string in the name of the provider. | Поисковая строка. Ищется провайдер по вхождению этой строки в наименовании
     * провайдера.
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $searchString = null;
    /**
     * Constructor method for GetServiceProvidersRequestType
     * @uses GetServiceProvidersRequestType::setTargetAccountId()
     * @uses GetServiceProvidersRequestType::setSearchString()
     * @param int $targetAccountId
     * @param string $searchString
     */
    public function __construct(?int $targetAccountId = null, ?string $searchString = null)
    {
        $this
            ->setTargetAccountId($targetAccountId)
            ->setSearchString($searchString);
    }
    /**
     * Get targetAccountId value
     * @return int|null
     */
    public function getTargetAccountId(): ?int
    {
        return $this->targetAccountId;
    }
    /**
     * Set targetAccountId value
     * @param int $targetAccountId
     * @return \MonetaServiceProviders\StructType\GetServiceProvidersRequestType
     */
    public function setTargetAccountId(?int $targetAccountId = null): self
    {
        // validation for constraint: int
        if (!is_null($targetAccountId) && !(is_int($targetAccountId) || ctype_digit($targetAccountId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($targetAccountId, true), gettype($targetAccountId)), __LINE__);
        }
        // validation for constraint: minInclusive(1)
        if (!is_null($targetAccountId) && $targetAccountId < 1) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, the value must be numerically greater than or equal to 1', var_export($targetAccountId, true)), __LINE__);
        }
        // validation for constraint: totalDigits(10)
        if (!is_null($targetAccountId) && mb_strlen(preg_replace('/(\D)/', '', (string) $targetAccountId)) > 10) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, the value must use at most 10 digits, "%d" given', var_export($targetAccountId, true), mb_strlen(preg_replace('/(\D)/', '', (string) $targetAccountId))), __LINE__);
        }
        $this->targetAccountId = $targetAccountId;
        
        return $this;
    }
    /**
     * Get searchString value
     * @return string|null
     */
    public function getSearchString(): ?string
    {
        return $this->searchString;
    }
    /**
     * Set searchString value
     * @param string $searchString
     * @return \MonetaServiceProviders\StructType\GetServiceProvidersRequestType
     */
    public function setSearchString(?string $searchString = null): self
    {
        // validation for constraint: string
        if (!is_null($searchString) && !is_string($searchString)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($searchString, true), gettype($searchString)), __LINE__);
        }
        $this->searchString = $searchString;
        
        return $this;
    }
}
