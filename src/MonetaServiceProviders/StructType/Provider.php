<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Provider StructType
 * Meta information extracted from the WSDL
 * - documentation: Описание провадера и его полей
 * @subpackage Structs
 */
class Provider extends AbstractStructBase
{
    /**
     * The id
     * @var string|null
     */
    protected ?string $id = null;
    /**
     * The targetAccountId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $targetAccountId = null;
    /**
     * The subProviderId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $subProviderId = null;
    /**
     * The name
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The limit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Limit|null
     */
    protected ?\MonetaServiceProviders\StructType\Limit $limit = null;
    /**
     * The fields
     * @var \MonetaServiceProviders\StructType\Fields|null
     */
    protected ?\MonetaServiceProviders\StructType\Fields $fields = null;
    /**
     * The fieldsets
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Fieldsets|null
     */
    protected ?\MonetaServiceProviders\StructType\Fieldsets $fieldsets = null;
    /**
     * The attributes
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Attributes|null
     */
    protected ?\MonetaServiceProviders\StructType\Attributes $attributes = null;
    /**
     * The active
     * Meta information extracted from the WSDL
     * - documentation: Флаг активности данного провайдера
     * - default: true
     * @var bool|null
     */
    protected ?bool $active = null;
    /**
     * The firstStep
     * Meta information extracted from the WSDL
     * - documentation: Первый шаг для данного параметра.
     * - default: PAY
     * @var string|null
     */
    protected ?string $firstStep = null;
    /**
     * Constructor method for Provider
     * @uses Provider::setId()
     * @uses Provider::setTargetAccountId()
     * @uses Provider::setSubProviderId()
     * @uses Provider::setName()
     * @uses Provider::setComment()
     * @uses Provider::setLimit()
     * @uses Provider::setFields()
     * @uses Provider::setFieldsets()
     * @uses Provider::setAttributes()
     * @uses Provider::setActive()
     * @uses Provider::setFirstStep()
     * @param string $id
     * @param int $targetAccountId
     * @param int $subProviderId
     * @param string $name
     * @param string $comment
     * @param \MonetaServiceProviders\StructType\Limit $limit
     * @param \MonetaServiceProviders\StructType\Fields $fields
     * @param \MonetaServiceProviders\StructType\Fieldsets $fieldsets
     * @param \MonetaServiceProviders\StructType\Attributes $attributes
     * @param bool $active
     * @param string $firstStep
     */
    public function __construct(?string $id = null, ?int $targetAccountId = null, ?int $subProviderId = null, ?string $name = null, ?string $comment = null, ?\MonetaServiceProviders\StructType\Limit $limit = null, ?\MonetaServiceProviders\StructType\Fields $fields = null, ?\MonetaServiceProviders\StructType\Fieldsets $fieldsets = null, ?\MonetaServiceProviders\StructType\Attributes $attributes = null, ?bool $active = true, ?string $firstStep = null)
    {
        $this
            ->setId($id)
            ->setTargetAccountId($targetAccountId)
            ->setSubProviderId($subProviderId)
            ->setName($name)
            ->setComment($comment)
            ->setLimit($limit)
            ->setFields($fields)
            ->setFieldsets($fieldsets)
            ->setAttributes($attributes)
            ->setActive($active)
            ->setFirstStep($firstStep);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get targetAccountId value
     * @return int|null
     */
    public function getTargetAccountId(): ?int
    {
        return $this->targetAccountId;
    }
    /**
     * Set targetAccountId value
     * @param int $targetAccountId
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setTargetAccountId(?int $targetAccountId = null): self
    {
        // validation for constraint: int
        if (!is_null($targetAccountId) && !(is_int($targetAccountId) || ctype_digit($targetAccountId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($targetAccountId, true), gettype($targetAccountId)), __LINE__);
        }
        $this->targetAccountId = $targetAccountId;
        
        return $this;
    }
    /**
     * Get subProviderId value
     * @return int|null
     */
    public function getSubProviderId(): ?int
    {
        return $this->subProviderId;
    }
    /**
     * Set subProviderId value
     * @param int $subProviderId
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setSubProviderId(?int $subProviderId = null): self
    {
        // validation for constraint: int
        if (!is_null($subProviderId) && !(is_int($subProviderId) || ctype_digit($subProviderId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($subProviderId, true), gettype($subProviderId)), __LINE__);
        }
        $this->subProviderId = $subProviderId;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get limit value
     * @return \MonetaServiceProviders\StructType\Limit|null
     */
    public function getLimit(): ?\MonetaServiceProviders\StructType\Limit
    {
        return $this->limit;
    }
    /**
     * Set limit value
     * @param \MonetaServiceProviders\StructType\Limit $limit
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setLimit(?\MonetaServiceProviders\StructType\Limit $limit = null): self
    {
        $this->limit = $limit;
        
        return $this;
    }
    /**
     * Get fields value
     * @return \MonetaServiceProviders\StructType\Fields|null
     */
    public function getFields(): ?\MonetaServiceProviders\StructType\Fields
    {
        return $this->fields;
    }
    /**
     * Set fields value
     * @param \MonetaServiceProviders\StructType\Fields $fields
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setFields(?\MonetaServiceProviders\StructType\Fields $fields = null): self
    {
        $this->fields = $fields;
        
        return $this;
    }
    /**
     * Get fieldsets value
     * @return \MonetaServiceProviders\StructType\Fieldsets|null
     */
    public function getFieldsets(): ?\MonetaServiceProviders\StructType\Fieldsets
    {
        return $this->fieldsets;
    }
    /**
     * Set fieldsets value
     * @param \MonetaServiceProviders\StructType\Fieldsets $fieldsets
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setFieldsets(?\MonetaServiceProviders\StructType\Fieldsets $fieldsets = null): self
    {
        $this->fieldsets = $fieldsets;
        
        return $this;
    }
    /**
     * Get attributes value
     * @return \MonetaServiceProviders\StructType\Attributes|null
     */
    public function getAttributes(): ?\MonetaServiceProviders\StructType\Attributes
    {
        return $this->attributes;
    }
    /**
     * Set attributes value
     * @param \MonetaServiceProviders\StructType\Attributes $attributes
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setAttributes(?\MonetaServiceProviders\StructType\Attributes $attributes = null): self
    {
        $this->attributes = $attributes;
        
        return $this;
    }
    /**
     * Get active value
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }
    /**
     * Set active value
     * @param bool $active
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setActive(?bool $active = true): self
    {
        // validation for constraint: boolean
        if (!is_null($active) && !is_bool($active)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($active, true), gettype($active)), __LINE__);
        }
        $this->active = $active;
        
        return $this;
    }
    /**
     * Get firstStep value
     * @return string|null
     */
    public function getFirstStep(): ?string
    {
        return $this->firstStep;
    }
    /**
     * Set firstStep value
     * @uses \MonetaServiceProviders\EnumType\Step::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Step::getValidValues()
     * @throws InvalidArgumentException
     * @param string $firstStep
     * @return \MonetaServiceProviders\StructType\Provider
     */
    public function setFirstStep(?string $firstStep = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\Step::valueIsValid($firstStep)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Step', is_array($firstStep) ? implode(', ', $firstStep) : var_export($firstStep, true), implode(', ', \MonetaServiceProviders\EnumType\Step::getValidValues())), __LINE__);
        }
        $this->firstStep = $firstStep;
        
        return $this;
    }
}
