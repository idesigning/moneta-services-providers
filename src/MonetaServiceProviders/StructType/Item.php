<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for item StructType
 * @subpackage Structs
 */
class Item extends AbstractStructBase
{
    /**
     * The _
     * @var string|null
     */
    protected ?string $_ = null;
    /**
     * The id
     * @var string|null
     */
    protected ?string $id = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - documentation: Сумма к оплате при выборе данного пункта | Тип, представляющий значение суммы в операции. | Type representing transaction amounts.
     * - base: xsd:decimal
     * - fractionDigits: 2
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * Constructor method for item
     * @uses Item::set_()
     * @uses Item::setId()
     * @uses Item::setAmount()
     * @param string $_
     * @param string $id
     * @param float $amount
     */
    public function __construct(?string $_ = null, ?string $id = null, ?float $amount = null)
    {
        $this
            ->set_($_)
            ->setId($id)
            ->setAmount($amount);
    }
    /**
     * Get _ value
     * @return string|null
     */
    public function get_(): ?string
    {
        return $this->_;
    }
    /**
     * Set _ value
     * @param string $_
     * @return \MonetaServiceProviders\StructType\Item
     */
    public function set_(?string $_ = null): self
    {
        // validation for constraint: string
        if (!is_null($_) && !is_string($_)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($_, true), gettype($_)), __LINE__);
        }
        $this->_ = $_;
        
        return $this;
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \MonetaServiceProviders\StructType\Item
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \MonetaServiceProviders\StructType\Item
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        // validation for constraint: fractionDigits(2)
        if (!is_null($amount) && mb_strlen(mb_substr((string) $amount, false !== mb_strpos((string) $amount, '.') ? mb_strpos((string) $amount, '.') + 1 : mb_strlen((string) $amount))) > 2) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, the value must at most contain 2 fraction digits, %d given', var_export($amount, true), mb_strlen(mb_substr((string) $amount, mb_strpos((string) $amount, '.') + 1))), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
}
