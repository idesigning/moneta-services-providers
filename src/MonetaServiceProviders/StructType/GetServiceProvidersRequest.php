<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetServiceProvidersRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Получение списка провайдеров по определенному номеру счета
 * @subpackage Structs
 */
class GetServiceProvidersRequest extends GetServiceProvidersRequestType
{
}
