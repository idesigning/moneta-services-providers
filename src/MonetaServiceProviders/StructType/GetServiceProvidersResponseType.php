<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetServiceProvidersResponseType StructType
 * @subpackage Structs
 */
class GetServiceProvidersResponseType extends AbstractStructBase
{
    /**
     * The providerInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\ProviderInfo[]
     */
    protected ?array $providerInfo = null;
    /**
     * Constructor method for GetServiceProvidersResponseType
     * @uses GetServiceProvidersResponseType::setProviderInfo()
     * @param \MonetaServiceProviders\StructType\ProviderInfo[] $providerInfo
     */
    public function __construct(?array $providerInfo = null)
    {
        $this
            ->setProviderInfo($providerInfo);
    }
    /**
     * Get providerInfo value
     * @return \MonetaServiceProviders\StructType\ProviderInfo[]
     */
    public function getProviderInfo(): ?array
    {
        return $this->providerInfo;
    }
    /**
     * This method is responsible for validating the values passed to the setProviderInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setProviderInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateProviderInfoForArrayConstraintsFromSetProviderInfo(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $getServiceProvidersResponseTypeProviderInfoItem) {
            // validation for constraint: itemType
            if (!$getServiceProvidersResponseTypeProviderInfoItem instanceof \MonetaServiceProviders\StructType\ProviderInfo) {
                $invalidValues[] = is_object($getServiceProvidersResponseTypeProviderInfoItem) ? get_class($getServiceProvidersResponseTypeProviderInfoItem) : sprintf('%s(%s)', gettype($getServiceProvidersResponseTypeProviderInfoItem), var_export($getServiceProvidersResponseTypeProviderInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The providerInfo property can only contain items of type \MonetaServiceProviders\StructType\ProviderInfo, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set providerInfo value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\ProviderInfo[] $providerInfo
     * @return \MonetaServiceProviders\StructType\GetServiceProvidersResponseType
     */
    public function setProviderInfo(?array $providerInfo = null): self
    {
        // validation for constraint: array
        if ('' !== ($providerInfoArrayErrorMessage = self::validateProviderInfoForArrayConstraintsFromSetProviderInfo($providerInfo))) {
            throw new InvalidArgumentException($providerInfoArrayErrorMessage, __LINE__);
        }
        $this->providerInfo = $providerInfo;
        
        return $this;
    }
    /**
     * Add item to providerInfo value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\ProviderInfo $item
     * @return \MonetaServiceProviders\StructType\GetServiceProvidersResponseType
     */
    public function addToProviderInfo(\MonetaServiceProviders\StructType\ProviderInfo $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\ProviderInfo) {
            throw new InvalidArgumentException(sprintf('The providerInfo property can only contain items of type \MonetaServiceProviders\StructType\ProviderInfo, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->providerInfo[] = $item;
        
        return $this;
    }
}
