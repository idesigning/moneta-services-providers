<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNextStepResponseType StructType
 * @subpackage Structs
 */
class GetNextStepResponseType extends AbstractStructBase
{
    /**
     * The providerId
     * Meta information extracted from the WSDL
     * - documentation: Идентификатор провайдера в системе MONETA.RU
     * @var string|null
     */
    protected ?string $providerId = null;
    /**
     * The nextStep
     * Meta information extracted from the WSDL
     * - documentation: Следующий шаг ввода параметров для данного провайдера. Если переданный шаг последний (PAY), то в ответе при осутствиии ошибок
     * тоже придет PAY
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nextStep = null;
    /**
     * The fields
     * Meta information extracted from the WSDL
     * - documentation: Поля требуемые для ввода на шаге nextStep
     * @var \MonetaServiceProviders\StructType\Fields|null
     */
    protected ?\MonetaServiceProviders\StructType\Fields $fields = null;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - documentation: Сумма к оплате, если она возвращается провайдером | Тип, представляющий значение суммы в операции. | Type representing transaction amounts.
     * - base: xsd:decimal
     * - fractionDigits: 2
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $amount = null;
    /**
     * Constructor method for GetNextStepResponseType
     * @uses GetNextStepResponseType::setProviderId()
     * @uses GetNextStepResponseType::setNextStep()
     * @uses GetNextStepResponseType::setFields()
     * @uses GetNextStepResponseType::setAmount()
     * @param string $providerId
     * @param string $nextStep
     * @param \MonetaServiceProviders\StructType\Fields $fields
     * @param float $amount
     */
    public function __construct(?string $providerId = null, ?string $nextStep = null, ?\MonetaServiceProviders\StructType\Fields $fields = null, ?float $amount = null)
    {
        $this
            ->setProviderId($providerId)
            ->setNextStep($nextStep)
            ->setFields($fields)
            ->setAmount($amount);
    }
    /**
     * Get providerId value
     * @return string|null
     */
    public function getProviderId(): ?string
    {
        return $this->providerId;
    }
    /**
     * Set providerId value
     * @param string $providerId
     * @return \MonetaServiceProviders\StructType\GetNextStepResponseType
     */
    public function setProviderId(?string $providerId = null): self
    {
        // validation for constraint: string
        if (!is_null($providerId) && !is_string($providerId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($providerId, true), gettype($providerId)), __LINE__);
        }
        $this->providerId = $providerId;
        
        return $this;
    }
    /**
     * Get nextStep value
     * @return string|null
     */
    public function getNextStep(): ?string
    {
        return $this->nextStep;
    }
    /**
     * Set nextStep value
     * @uses \MonetaServiceProviders\EnumType\Step::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Step::getValidValues()
     * @throws InvalidArgumentException
     * @param string $nextStep
     * @return \MonetaServiceProviders\StructType\GetNextStepResponseType
     */
    public function setNextStep(?string $nextStep = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\Step::valueIsValid($nextStep)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Step', is_array($nextStep) ? implode(', ', $nextStep) : var_export($nextStep, true), implode(', ', \MonetaServiceProviders\EnumType\Step::getValidValues())), __LINE__);
        }
        $this->nextStep = $nextStep;
        
        return $this;
    }
    /**
     * Get fields value
     * @return \MonetaServiceProviders\StructType\Fields|null
     */
    public function getFields(): ?\MonetaServiceProviders\StructType\Fields
    {
        return $this->fields;
    }
    /**
     * Set fields value
     * @param \MonetaServiceProviders\StructType\Fields $fields
     * @return \MonetaServiceProviders\StructType\GetNextStepResponseType
     */
    public function setFields(?\MonetaServiceProviders\StructType\Fields $fields = null): self
    {
        $this->fields = $fields;
        
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \MonetaServiceProviders\StructType\GetNextStepResponseType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        // validation for constraint: fractionDigits(2)
        if (!is_null($amount) && mb_strlen(mb_substr((string) $amount, false !== mb_strpos((string) $amount, '.') ? mb_strpos((string) $amount, '.') + 1 : mb_strlen((string) $amount))) > 2) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, the value must at most contain 2 fraction digits, %d given', var_export($amount, true), mb_strlen(mb_substr((string) $amount, mb_strpos((string) $amount, '.') + 1))), __LINE__);
        }
        $this->amount = $amount;
        
        return $this;
    }
}
