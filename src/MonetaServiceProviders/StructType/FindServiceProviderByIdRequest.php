<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FindServiceProviderByIdRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Запрос на поиск провайдера по его идентификаторы. Если провайдер не найден, то возникает Exception.
 * @subpackage Structs
 */
class FindServiceProviderByIdRequest extends FindServiceProviderByIdRequestType
{
}
