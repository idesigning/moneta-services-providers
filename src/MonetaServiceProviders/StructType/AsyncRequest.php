<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AsyncRequest StructType
 * @subpackage Structs
 */
class AsyncRequest extends AbstractStructBase
{
    /**
     * The FindServiceProviderByIdRequest
     * Meta information extracted from the WSDL
     * - choice: FindServiceProviderByIdRequest | GetNextStepRequest
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - ref: tns:FindServiceProviderByIdRequest
     * @var \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest|null
     */
    protected ?\MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $FindServiceProviderByIdRequest = null;
    /**
     * The GetNextStepRequest
     * Meta information extracted from the WSDL
     * - choice: FindServiceProviderByIdRequest | GetNextStepRequest
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - ref: tns:GetNextStepRequest
     * @var \MonetaServiceProviders\StructType\GetNextStepRequest|null
     */
    protected ?\MonetaServiceProviders\StructType\GetNextStepRequest $GetNextStepRequest = null;
    /**
     * The callbackUrl
     * Meta information extracted from the WSDL
     * - documentation: Url который будет вызван, после завершения асинхронной задачи (перехода её в терминальное сосотояние)
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $callbackUrl = null;
    /**
     * The asyncId
     * Meta information extracted from the WSDL
     * - choice: asyncId
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * @var int|null
     */
    protected ?int $asyncId = null;
    /**
     * Constructor method for AsyncRequest
     * @uses AsyncRequest::setFindServiceProviderByIdRequest()
     * @uses AsyncRequest::setGetNextStepRequest()
     * @uses AsyncRequest::setCallbackUrl()
     * @uses AsyncRequest::setAsyncId()
     * @param \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest
     * @param \MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest
     * @param string $callbackUrl
     * @param int $asyncId
     */
    public function __construct(?\MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest = null, ?\MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest = null, ?string $callbackUrl = null, ?int $asyncId = null)
    {
        $this
            ->setFindServiceProviderByIdRequest($findServiceProviderByIdRequest)
            ->setGetNextStepRequest($getNextStepRequest)
            ->setCallbackUrl($callbackUrl)
            ->setAsyncId($asyncId);
    }
    /**
     * Get FindServiceProviderByIdRequest value
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest|null
     */
    public function getFindServiceProviderByIdRequest(): ?\MonetaServiceProviders\StructType\FindServiceProviderByIdRequest
    {
        return isset($this->FindServiceProviderByIdRequest) ? $this->FindServiceProviderByIdRequest : null;
    }
    /**
     * This method is responsible for validating the value passed to the setFindServiceProviderByIdRequest method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFindServiceProviderByIdRequest method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateFindServiceProviderByIdRequestForChoiceConstraintsFromSetFindServiceProviderByIdRequest($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'GetNextStepRequest',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property FindServiceProviderByIdRequest can\'t be set as the property %s is already set. Only one property must be set among these properties: FindServiceProviderByIdRequest, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set FindServiceProviderByIdRequest value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest
     * @return \MonetaServiceProviders\StructType\AsyncRequest
     */
    public function setFindServiceProviderByIdRequest(?\MonetaServiceProviders\StructType\FindServiceProviderByIdRequest $findServiceProviderByIdRequest = null): self
    {
        // validation for constraint: choice(FindServiceProviderByIdRequest, GetNextStepRequest)
        if ('' !== ($findServiceProviderByIdRequestChoiceErrorMessage = self::validateFindServiceProviderByIdRequestForChoiceConstraintsFromSetFindServiceProviderByIdRequest($findServiceProviderByIdRequest))) {
            throw new InvalidArgumentException($findServiceProviderByIdRequestChoiceErrorMessage, __LINE__);
        }
        if (is_null($findServiceProviderByIdRequest) || (is_array($findServiceProviderByIdRequest) && empty($findServiceProviderByIdRequest))) {
            unset($this->FindServiceProviderByIdRequest);
        } else {
            $this->FindServiceProviderByIdRequest = $findServiceProviderByIdRequest;
        }
        
        return $this;
    }
    /**
     * Get GetNextStepRequest value
     * @return \MonetaServiceProviders\StructType\GetNextStepRequest|null
     */
    public function getGetNextStepRequest(): ?\MonetaServiceProviders\StructType\GetNextStepRequest
    {
        return isset($this->GetNextStepRequest) ? $this->GetNextStepRequest : null;
    }
    /**
     * This method is responsible for validating the value passed to the setGetNextStepRequest method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGetNextStepRequest method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateGetNextStepRequestForChoiceConstraintsFromSetGetNextStepRequest($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'FindServiceProviderByIdRequest',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property GetNextStepRequest can\'t be set as the property %s is already set. Only one property must be set among these properties: GetNextStepRequest, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set GetNextStepRequest value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest
     * @return \MonetaServiceProviders\StructType\AsyncRequest
     */
    public function setGetNextStepRequest(?\MonetaServiceProviders\StructType\GetNextStepRequest $getNextStepRequest = null): self
    {
        // validation for constraint: choice(FindServiceProviderByIdRequest, GetNextStepRequest)
        if ('' !== ($getNextStepRequestChoiceErrorMessage = self::validateGetNextStepRequestForChoiceConstraintsFromSetGetNextStepRequest($getNextStepRequest))) {
            throw new InvalidArgumentException($getNextStepRequestChoiceErrorMessage, __LINE__);
        }
        if (is_null($getNextStepRequest) || (is_array($getNextStepRequest) && empty($getNextStepRequest))) {
            unset($this->GetNextStepRequest);
        } else {
            $this->GetNextStepRequest = $getNextStepRequest;
        }
        
        return $this;
    }
    /**
     * Get callbackUrl value
     * @return string|null
     */
    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }
    /**
     * Set callbackUrl value
     * @param string $callbackUrl
     * @return \MonetaServiceProviders\StructType\AsyncRequest
     */
    public function setCallbackUrl(?string $callbackUrl = null): self
    {
        // validation for constraint: string
        if (!is_null($callbackUrl) && !is_string($callbackUrl)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($callbackUrl, true), gettype($callbackUrl)), __LINE__);
        }
        $this->callbackUrl = $callbackUrl;
        
        return $this;
    }
    /**
     * Get asyncId value
     * @return int|null
     */
    public function getAsyncId(): ?int
    {
        return isset($this->asyncId) ? $this->asyncId : null;
    }
    /**
     * This method is responsible for validating the value passed to the setAsyncId method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAsyncId method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateAsyncIdForChoiceConstraintsFromSetAsyncId($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property asyncId can\'t be set as the property %s is already set. Only one property must be set among these properties: asyncId, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set asyncId value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param int $asyncId
     * @return \MonetaServiceProviders\StructType\AsyncRequest
     */
    public function setAsyncId(?int $asyncId = null): self
    {
        // validation for constraint: int
        if (!is_null($asyncId) && !(is_int($asyncId) || ctype_digit($asyncId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($asyncId, true), gettype($asyncId)), __LINE__);
        }
        // validation for constraint: choice(asyncId)
        if ('' !== ($asyncIdChoiceErrorMessage = self::validateAsyncIdForChoiceConstraintsFromSetAsyncId($asyncId))) {
            throw new InvalidArgumentException($asyncIdChoiceErrorMessage, __LINE__);
        }
        if (is_null($asyncId) || (is_array($asyncId) && empty($asyncId))) {
            unset($this->asyncId);
        } else {
            $this->asyncId = $asyncId;
        }
        
        return $this;
    }
}
