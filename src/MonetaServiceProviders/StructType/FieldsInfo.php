<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FieldsInfo StructType
 * Meta information extracted from the WSDL
 * - documentation: Контейнер для NameValueAttribute.
 * @subpackage Structs
 */
class FieldsInfo extends AbstractStructBase
{
    /**
     * The attribute
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\NameValueAttribute[]
     */
    protected ?array $attribute = null;
    /**
     * Constructor method for FieldsInfo
     * @uses FieldsInfo::setAttribute()
     * @param \MonetaServiceProviders\StructType\NameValueAttribute[] $attribute
     */
    public function __construct(?array $attribute = null)
    {
        $this
            ->setAttribute($attribute);
    }
    /**
     * Get attribute value
     * @return \MonetaServiceProviders\StructType\NameValueAttribute[]
     */
    public function getAttribute(): ?array
    {
        return $this->attribute;
    }
    /**
     * This method is responsible for validating the values passed to the setAttribute method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAttribute method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAttributeForArrayConstraintsFromSetAttribute(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fieldsInfoAttributeItem) {
            // validation for constraint: itemType
            if (!$fieldsInfoAttributeItem instanceof \MonetaServiceProviders\StructType\NameValueAttribute) {
                $invalidValues[] = is_object($fieldsInfoAttributeItem) ? get_class($fieldsInfoAttributeItem) : sprintf('%s(%s)', gettype($fieldsInfoAttributeItem), var_export($fieldsInfoAttributeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The attribute property can only contain items of type \MonetaServiceProviders\StructType\NameValueAttribute, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set attribute value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\NameValueAttribute[] $attribute
     * @return \MonetaServiceProviders\StructType\FieldsInfo
     */
    public function setAttribute(?array $attribute = null): self
    {
        // validation for constraint: array
        if ('' !== ($attributeArrayErrorMessage = self::validateAttributeForArrayConstraintsFromSetAttribute($attribute))) {
            throw new InvalidArgumentException($attributeArrayErrorMessage, __LINE__);
        }
        $this->attribute = $attribute;
        
        return $this;
    }
    /**
     * Add item to attribute value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\NameValueAttribute $item
     * @return \MonetaServiceProviders\StructType\FieldsInfo
     */
    public function addToAttribute(\MonetaServiceProviders\StructType\NameValueAttribute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\NameValueAttribute) {
            throw new InvalidArgumentException(sprintf('The attribute property can only contain items of type \MonetaServiceProviders\StructType\NameValueAttribute, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->attribute[] = $item;
        
        return $this;
    }
}
