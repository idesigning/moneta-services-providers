<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AsyncResponse StructType
 * @subpackage Structs
 */
class AsyncResponse extends AbstractStructBase
{
    /**
     * The FindServiceProviderByIdResponse
     * Meta information extracted from the WSDL
     * - choice: FindServiceProviderByIdResponse | GetNextStepResponse
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - ref: tns:FindServiceProviderByIdResponse
     * @var \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse|null
     */
    protected ?\MonetaServiceProviders\StructType\FindServiceProviderByIdResponse $FindServiceProviderByIdResponse = null;
    /**
     * The GetNextStepResponse
     * Meta information extracted from the WSDL
     * - choice: FindServiceProviderByIdResponse | GetNextStepResponse
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - ref: tns:GetNextStepResponse
     * @var \MonetaServiceProviders\StructType\GetNextStepResponse|null
     */
    protected ?\MonetaServiceProviders\StructType\GetNextStepResponse $GetNextStepResponse = null;
    /**
     * The asyncId
     * @var int|null
     */
    protected ?int $asyncId = null;
    /**
     * The asyncStatus
     * @var string|null
     */
    protected ?string $asyncStatus = null;
    /**
     * The expirationDate
     * @var string|null
     */
    protected ?string $expirationDate = null;
    /**
     * Constructor method for AsyncResponse
     * @uses AsyncResponse::setFindServiceProviderByIdResponse()
     * @uses AsyncResponse::setGetNextStepResponse()
     * @uses AsyncResponse::setAsyncId()
     * @uses AsyncResponse::setAsyncStatus()
     * @uses AsyncResponse::setExpirationDate()
     * @param \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse $findServiceProviderByIdResponse
     * @param \MonetaServiceProviders\StructType\GetNextStepResponse $getNextStepResponse
     * @param int $asyncId
     * @param string $asyncStatus
     * @param string $expirationDate
     */
    public function __construct(?\MonetaServiceProviders\StructType\FindServiceProviderByIdResponse $findServiceProviderByIdResponse = null, ?\MonetaServiceProviders\StructType\GetNextStepResponse $getNextStepResponse = null, ?int $asyncId = null, ?string $asyncStatus = null, ?string $expirationDate = null)
    {
        $this
            ->setFindServiceProviderByIdResponse($findServiceProviderByIdResponse)
            ->setGetNextStepResponse($getNextStepResponse)
            ->setAsyncId($asyncId)
            ->setAsyncStatus($asyncStatus)
            ->setExpirationDate($expirationDate);
    }
    /**
     * Get FindServiceProviderByIdResponse value
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse|null
     */
    public function getFindServiceProviderByIdResponse(): ?\MonetaServiceProviders\StructType\FindServiceProviderByIdResponse
    {
        return isset($this->FindServiceProviderByIdResponse) ? $this->FindServiceProviderByIdResponse : null;
    }
    /**
     * This method is responsible for validating the value passed to the setFindServiceProviderByIdResponse method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFindServiceProviderByIdResponse method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateFindServiceProviderByIdResponseForChoiceConstraintsFromSetFindServiceProviderByIdResponse($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'GetNextStepResponse',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property FindServiceProviderByIdResponse can\'t be set as the property %s is already set. Only one property must be set among these properties: FindServiceProviderByIdResponse, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set FindServiceProviderByIdResponse value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\FindServiceProviderByIdResponse $findServiceProviderByIdResponse
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function setFindServiceProviderByIdResponse(?\MonetaServiceProviders\StructType\FindServiceProviderByIdResponse $findServiceProviderByIdResponse = null): self
    {
        // validation for constraint: choice(FindServiceProviderByIdResponse, GetNextStepResponse)
        if ('' !== ($findServiceProviderByIdResponseChoiceErrorMessage = self::validateFindServiceProviderByIdResponseForChoiceConstraintsFromSetFindServiceProviderByIdResponse($findServiceProviderByIdResponse))) {
            throw new InvalidArgumentException($findServiceProviderByIdResponseChoiceErrorMessage, __LINE__);
        }
        if (is_null($findServiceProviderByIdResponse) || (is_array($findServiceProviderByIdResponse) && empty($findServiceProviderByIdResponse))) {
            unset($this->FindServiceProviderByIdResponse);
        } else {
            $this->FindServiceProviderByIdResponse = $findServiceProviderByIdResponse;
        }
        
        return $this;
    }
    /**
     * Get GetNextStepResponse value
     * @return \MonetaServiceProviders\StructType\GetNextStepResponse|null
     */
    public function getGetNextStepResponse(): ?\MonetaServiceProviders\StructType\GetNextStepResponse
    {
        return isset($this->GetNextStepResponse) ? $this->GetNextStepResponse : null;
    }
    /**
     * This method is responsible for validating the value passed to the setGetNextStepResponse method
     * This method is willingly generated in order to preserve the one-line inline validation within the setGetNextStepResponse method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateGetNextStepResponseForChoiceConstraintsFromSetGetNextStepResponse($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'FindServiceProviderByIdResponse',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property GetNextStepResponse can\'t be set as the property %s is already set. Only one property must be set among these properties: GetNextStepResponse, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set GetNextStepResponse value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\GetNextStepResponse $getNextStepResponse
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function setGetNextStepResponse(?\MonetaServiceProviders\StructType\GetNextStepResponse $getNextStepResponse = null): self
    {
        // validation for constraint: choice(FindServiceProviderByIdResponse, GetNextStepResponse)
        if ('' !== ($getNextStepResponseChoiceErrorMessage = self::validateGetNextStepResponseForChoiceConstraintsFromSetGetNextStepResponse($getNextStepResponse))) {
            throw new InvalidArgumentException($getNextStepResponseChoiceErrorMessage, __LINE__);
        }
        if (is_null($getNextStepResponse) || (is_array($getNextStepResponse) && empty($getNextStepResponse))) {
            unset($this->GetNextStepResponse);
        } else {
            $this->GetNextStepResponse = $getNextStepResponse;
        }
        
        return $this;
    }
    /**
     * Get asyncId value
     * @return int|null
     */
    public function getAsyncId(): ?int
    {
        return $this->asyncId;
    }
    /**
     * Set asyncId value
     * @param int $asyncId
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function setAsyncId(?int $asyncId = null): self
    {
        // validation for constraint: int
        if (!is_null($asyncId) && !(is_int($asyncId) || ctype_digit($asyncId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($asyncId, true), gettype($asyncId)), __LINE__);
        }
        $this->asyncId = $asyncId;
        
        return $this;
    }
    /**
     * Get asyncStatus value
     * @return string|null
     */
    public function getAsyncStatus(): ?string
    {
        return $this->asyncStatus;
    }
    /**
     * Set asyncStatus value
     * @uses \MonetaServiceProviders\EnumType\AsyncStatus::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\AsyncStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $asyncStatus
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function setAsyncStatus(?string $asyncStatus = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\AsyncStatus::valueIsValid($asyncStatus)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\AsyncStatus', is_array($asyncStatus) ? implode(', ', $asyncStatus) : var_export($asyncStatus, true), implode(', ', \MonetaServiceProviders\EnumType\AsyncStatus::getValidValues())), __LINE__);
        }
        $this->asyncStatus = $asyncStatus;
        
        return $this;
    }
    /**
     * Get expirationDate value
     * @return string|null
     */
    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }
    /**
     * Set expirationDate value
     * @param string $expirationDate
     * @return \MonetaServiceProviders\StructType\AsyncResponse
     */
    public function setExpirationDate(?string $expirationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expirationDate) && !is_string($expirationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expirationDate, true), gettype($expirationDate)), __LINE__);
        }
        $this->expirationDate = $expirationDate;
        
        return $this;
    }
}
