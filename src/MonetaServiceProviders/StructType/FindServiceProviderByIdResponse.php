<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FindServiceProviderByIdResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Ответ, который приходит на запрос FindServiceProviderByIdRequest. В ответе содержится информация по найденному провайдеру.
 * @subpackage Structs
 */
class FindServiceProviderByIdResponse extends FindServiceProviderByIdResponseType
{
}
