<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Entity StructType
 * Meta information extracted from the WSDL
 * - documentation: Base type containing "version" attribute. | Базовый тип, содержащий атрибут "version".
 * @subpackage Structs
 */
class Entity extends AbstractStructBase
{
    /**
     * The version
     * Meta information extracted from the WSDL
     * - default: VERSION_1
     * - use: optional
     * @var string|null
     */
    protected ?string $version = null;
    /**
     * Constructor method for Entity
     * @uses Entity::setVersion()
     * @param string $version
     */
    public function __construct(?string $version = null)
    {
        $this
            ->setVersion($version);
    }
    /**
     * Get version value
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }
    /**
     * Set version value
     * @uses \MonetaServiceProviders\EnumType\Version::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Version::getValidValues()
     * @throws InvalidArgumentException
     * @param string $version
     * @return \MonetaServiceProviders\StructType\Entity
     */
    public function setVersion(?string $version = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\Version::valueIsValid($version)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Version', is_array($version) ? implode(', ', $version) : var_export($version, true), implode(', ', \MonetaServiceProviders\EnumType\Version::getValidValues())), __LINE__);
        }
        $this->version = $version;
        
        return $this;
    }
}
