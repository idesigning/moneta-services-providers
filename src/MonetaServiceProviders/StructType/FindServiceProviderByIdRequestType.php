<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FindServiceProviderByIdRequestType StructType
 * @subpackage Structs
 */
class FindServiceProviderByIdRequestType extends Entity
{
    /**
     * The providerId
     * Meta information extracted from the WSDL
     * - documentation: Идентификатор провайдера в системе MONETA.RU
     * @var string|null
     */
    protected ?string $providerId = null;
    /**
     * Constructor method for FindServiceProviderByIdRequestType
     * @uses FindServiceProviderByIdRequestType::setProviderId()
     * @param string $providerId
     */
    public function __construct(?string $providerId = null)
    {
        $this
            ->setProviderId($providerId);
    }
    /**
     * Get providerId value
     * @return string|null
     */
    public function getProviderId(): ?string
    {
        return $this->providerId;
    }
    /**
     * Set providerId value
     * @param string $providerId
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdRequestType
     */
    public function setProviderId(?string $providerId = null): self
    {
        // validation for constraint: string
        if (!is_null($providerId) && !is_string($providerId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($providerId, true), gettype($providerId)), __LINE__);
        }
        $this->providerId = $providerId;
        
        return $this;
    }
}
