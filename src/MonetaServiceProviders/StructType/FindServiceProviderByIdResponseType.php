<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FindServiceProviderByIdResponseType StructType
 * @subpackage Structs
 */
class FindServiceProviderByIdResponseType extends AbstractStructBase
{
    /**
     * The provider
     * @var \MonetaServiceProviders\StructType\Provider|null
     */
    protected ?\MonetaServiceProviders\StructType\Provider $provider = null;
    /**
     * Constructor method for FindServiceProviderByIdResponseType
     * @uses FindServiceProviderByIdResponseType::setProvider()
     * @param \MonetaServiceProviders\StructType\Provider $provider
     */
    public function __construct(?\MonetaServiceProviders\StructType\Provider $provider = null)
    {
        $this
            ->setProvider($provider);
    }
    /**
     * Get provider value
     * @return \MonetaServiceProviders\StructType\Provider|null
     */
    public function getProvider(): ?\MonetaServiceProviders\StructType\Provider
    {
        return $this->provider;
    }
    /**
     * Set provider value
     * @param \MonetaServiceProviders\StructType\Provider $provider
     * @return \MonetaServiceProviders\StructType\FindServiceProviderByIdResponseType
     */
    public function setProvider(?\MonetaServiceProviders\StructType\Provider $provider = null): self
    {
        $this->provider = $provider;
        
        return $this;
    }
}
