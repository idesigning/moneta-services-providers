<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for fieldset StructType
 * @subpackage Structs
 */
class Fieldset extends AbstractStructBase
{
    /**
     * The fieldId
     * Meta information extracted from the WSDL
     * - documentation: Идентификатор поля
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var int[]
     */
    protected ?array $fieldId = null;
    /**
     * The label
     * Meta information extracted from the WSDL
     * - documentation: Наименование группы полей
     * @var string|null
     */
    protected ?string $label = null;
    /**
     * Constructor method for fieldset
     * @uses Fieldset::setFieldId()
     * @uses Fieldset::setLabel()
     * @param int[] $fieldId
     * @param string $label
     */
    public function __construct(?array $fieldId = null, ?string $label = null)
    {
        $this
            ->setFieldId($fieldId)
            ->setLabel($label);
    }
    /**
     * Get fieldId value
     * @return int[]
     */
    public function getFieldId(): ?array
    {
        return $this->fieldId;
    }
    /**
     * This method is responsible for validating the values passed to the setFieldId method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFieldId method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFieldIdForArrayConstraintsFromSetFieldId(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fieldsetFieldIdItem) {
            // validation for constraint: itemType
            if (!(is_int($fieldsetFieldIdItem) || ctype_digit($fieldsetFieldIdItem))) {
                $invalidValues[] = is_object($fieldsetFieldIdItem) ? get_class($fieldsetFieldIdItem) : sprintf('%s(%s)', gettype($fieldsetFieldIdItem), var_export($fieldsetFieldIdItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The fieldId property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set fieldId value
     * @throws InvalidArgumentException
     * @param int[] $fieldId
     * @return \MonetaServiceProviders\StructType\Fieldset
     */
    public function setFieldId(?array $fieldId = null): self
    {
        // validation for constraint: array
        if ('' !== ($fieldIdArrayErrorMessage = self::validateFieldIdForArrayConstraintsFromSetFieldId($fieldId))) {
            throw new InvalidArgumentException($fieldIdArrayErrorMessage, __LINE__);
        }
        $this->fieldId = $fieldId;
        
        return $this;
    }
    /**
     * Add item to fieldId value
     * @throws InvalidArgumentException
     * @param int $item
     * @return \MonetaServiceProviders\StructType\Fieldset
     */
    public function addToFieldId(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf('The fieldId property can only contain items of type int, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->fieldId[] = $item;
        
        return $this;
    }
    /**
     * Get label value
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }
    /**
     * Set label value
     * @param string $label
     * @return \MonetaServiceProviders\StructType\Fieldset
     */
    public function setLabel(?string $label = null): self
    {
        // validation for constraint: string
        if (!is_null($label) && !is_string($label)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($label, true), gettype($label)), __LINE__);
        }
        $this->label = $label;
        
        return $this;
    }
}
