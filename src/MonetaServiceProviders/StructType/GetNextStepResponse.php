<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNextStepResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Ответ, который приходит на запрос GetNextStepRequest. В ответе содержится список полей для следующего шага.
 * @subpackage Structs
 */
class GetNextStepResponse extends GetNextStepResponseType
{
}
