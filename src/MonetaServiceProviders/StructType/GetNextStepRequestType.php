<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNextStepRequestType StructType
 * @subpackage Structs
 */
class GetNextStepRequestType extends Entity
{
    /**
     * The providerId
     * Meta information extracted from the WSDL
     * - documentation: Идентификатор провайдера в системе MONETA.RU
     * @var string|null
     */
    protected ?string $providerId = null;
    /**
     * The currentStep
     * Meta information extracted from the WSDL
     * - documentation: Текущий шаг. Необязательный параметр. В отсутствии его используется первый шаг провайдера.
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $currentStep = null;
    /**
     * The fieldsInfo
     * Meta information extracted from the WSDL
     * - documentation: Значения уже введенных полей (имя поля - значение)
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\FieldsInfo|null
     */
    protected ?\MonetaServiceProviders\StructType\FieldsInfo $fieldsInfo = null;
    /**
     * Constructor method for GetNextStepRequestType
     * @uses GetNextStepRequestType::setProviderId()
     * @uses GetNextStepRequestType::setCurrentStep()
     * @uses GetNextStepRequestType::setFieldsInfo()
     * @param string $providerId
     * @param string $currentStep
     * @param \MonetaServiceProviders\StructType\FieldsInfo $fieldsInfo
     */
    public function __construct(?string $providerId = null, ?string $currentStep = null, ?\MonetaServiceProviders\StructType\FieldsInfo $fieldsInfo = null)
    {
        $this
            ->setProviderId($providerId)
            ->setCurrentStep($currentStep)
            ->setFieldsInfo($fieldsInfo);
    }
    /**
     * Get providerId value
     * @return string|null
     */
    public function getProviderId(): ?string
    {
        return $this->providerId;
    }
    /**
     * Set providerId value
     * @param string $providerId
     * @return \MonetaServiceProviders\StructType\GetNextStepRequestType
     */
    public function setProviderId(?string $providerId = null): self
    {
        // validation for constraint: string
        if (!is_null($providerId) && !is_string($providerId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($providerId, true), gettype($providerId)), __LINE__);
        }
        $this->providerId = $providerId;
        
        return $this;
    }
    /**
     * Get currentStep value
     * @return string|null
     */
    public function getCurrentStep(): ?string
    {
        return $this->currentStep;
    }
    /**
     * Set currentStep value
     * @uses \MonetaServiceProviders\EnumType\Step::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Step::getValidValues()
     * @throws InvalidArgumentException
     * @param string $currentStep
     * @return \MonetaServiceProviders\StructType\GetNextStepRequestType
     */
    public function setCurrentStep(?string $currentStep = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\Step::valueIsValid($currentStep)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Step', is_array($currentStep) ? implode(', ', $currentStep) : var_export($currentStep, true), implode(', ', \MonetaServiceProviders\EnumType\Step::getValidValues())), __LINE__);
        }
        $this->currentStep = $currentStep;
        
        return $this;
    }
    /**
     * Get fieldsInfo value
     * @return \MonetaServiceProviders\StructType\FieldsInfo|null
     */
    public function getFieldsInfo(): ?\MonetaServiceProviders\StructType\FieldsInfo
    {
        return $this->fieldsInfo;
    }
    /**
     * Set fieldsInfo value
     * @param \MonetaServiceProviders\StructType\FieldsInfo $fieldsInfo
     * @return \MonetaServiceProviders\StructType\GetNextStepRequestType
     */
    public function setFieldsInfo(?\MonetaServiceProviders\StructType\FieldsInfo $fieldsInfo = null): self
    {
        $this->fieldsInfo = $fieldsInfo;
        
        return $this;
    }
}
