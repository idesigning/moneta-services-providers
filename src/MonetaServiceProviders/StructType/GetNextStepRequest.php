<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetNextStepRequest StructType
 * Meta information extracted from the WSDL
 * - documentation: Получение описания и значения полей для очередного шага в сервис провайдере. Используется для построения визарда по оплате.
 * @subpackage Structs
 */
class GetNextStepRequest extends GetNextStepRequestType
{
}
