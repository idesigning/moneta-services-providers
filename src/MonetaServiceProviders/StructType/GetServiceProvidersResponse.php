<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetServiceProvidersResponse StructType
 * Meta information extracted from the WSDL
 * - documentation: Ответ, который приходит на запрос GetServiceProvidersRequest. В ответе содержится список провайдеров по данному счету.
 * @subpackage Structs
 */
class GetServiceProvidersResponse extends GetServiceProvidersResponseType
{
}
