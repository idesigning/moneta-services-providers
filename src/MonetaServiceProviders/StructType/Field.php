<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Field StructType
 * @subpackage Structs
 */
class Field extends AbstractStructBase
{
    /**
     * The attribute_name
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     * @var string
     */
    protected string $attribute_name;
    /**
     * The steps
     * Meta information extracted from the WSDL
     * - default: PAY
     * - maxOccurs: 2
     * - minOccurs: 0
     * @var string[]
     */
    protected ?array $steps = null;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $value = null;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - documentation: Результат проверки значения поля на ошибки (mask, required)
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $error = null;
    /**
     * The label
     * Meta information extracted from the WSDL
     * - documentation: Наименование поля
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $label = null;
    /**
     * The mask
     * Meta information extracted from the WSDL
     * - documentation: Маска для ввода значения поля
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $mask = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - documentation: Комметарий к полю
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The dependency
     * Meta information extracted from the WSDL
     * - documentation: Зависимость поля от других полей на ECMA скрипте. Выражение позволяет вычислить, нужно ли показывать это поле при определенном
     * значении других полей. В скрипте значения вида {id} заменяются на значения полей с этим id.
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $dependency = null;
    /**
     * The enum
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Enum|null
     */
    protected ?\MonetaServiceProviders\StructType\Enum $enum = null;
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The readonly
     * Meta information extracted from the WSDL
     * - documentation: true - значение поля только для чтения, то есть конечный пользователь не должен иметь возможности его изменять
     * - default: false
     * @var bool|null
     */
    protected ?bool $readonly = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The orderBy
     * Meta information extracted from the WSDL
     * - documentation: Значение определят порядок отображения полей для конечного пользователя (сортировка)
     * @var int|null
     */
    protected ?int $orderBy = null;
    /**
     * The dateformat
     * @var string|null
     */
    protected ?string $dateformat = null;
    /**
     * The maxlength
     * @var int|null
     */
    protected ?int $maxlength = null;
    /**
     * The minlength
     * @var int|null
     */
    protected ?int $minlength = null;
    /**
     * The pattern
     * @var string|null
     */
    protected ?string $pattern = null;
    /**
     * The pair_attribute
     * @var int|null
     */
    protected ?int $pair_attribute = null;
    /**
     * The required
     * Meta information extracted from the WSDL
     * - default: true
     * @var bool|null
     */
    protected ?bool $required = null;
    /**
     * The hidden
     * Meta information extracted from the WSDL
     * - documentation: true - поле должно быть скрыто для конечного пользователя
     * - default: false
     * @var bool|null
     */
    protected ?bool $hidden = null;
    /**
     * The temporary
     * Meta information extracted from the WSDL
     * - documentation: true - "временное" поле, не будет сохряняться в операцию
     * - default: false
     * @var bool|null
     */
    protected ?bool $temporary = null;
    /**
     * The multiple
     * Meta information extracted from the WSDL
     * - documentation: Признак возможности указать несколько значений при type = ENUM. Значения указываются через ","
     * - default: false
     * @var bool|null
     */
    protected ?bool $multiple = null;
    /**
     * Constructor method for Field
     * @uses Field::setAttribute_name()
     * @uses Field::setSteps()
     * @uses Field::setValue()
     * @uses Field::setError()
     * @uses Field::setLabel()
     * @uses Field::setMask()
     * @uses Field::setComment()
     * @uses Field::setDependency()
     * @uses Field::setEnum()
     * @uses Field::setType()
     * @uses Field::setReadonly()
     * @uses Field::setId()
     * @uses Field::setOrderBy()
     * @uses Field::setDateformat()
     * @uses Field::setMaxlength()
     * @uses Field::setMinlength()
     * @uses Field::setPattern()
     * @uses Field::setPair_attribute()
     * @uses Field::setRequired()
     * @uses Field::setHidden()
     * @uses Field::setTemporary()
     * @uses Field::setMultiple()
     * @param string $attribute_name
     * @param string[] $steps
     * @param string $value
     * @param string $error
     * @param string $label
     * @param string $mask
     * @param string $comment
     * @param string $dependency
     * @param \MonetaServiceProviders\StructType\Enum $enum
     * @param string $type
     * @param bool $readonly
     * @param int $id
     * @param int $orderBy
     * @param string $dateformat
     * @param int $maxlength
     * @param int $minlength
     * @param string $pattern
     * @param int $pair_attribute
     * @param bool $required
     * @param bool $hidden
     * @param bool $temporary
     * @param bool $multiple
     */
    public function __construct(string $attribute_name, ?array $steps = null, ?string $value = null, ?string $error = null, ?string $label = null, ?string $mask = null, ?string $comment = null, ?string $dependency = null, ?\MonetaServiceProviders\StructType\Enum $enum = null, ?string $type = null, ?bool $readonly = false, ?int $id = null, ?int $orderBy = null, ?string $dateformat = null, ?int $maxlength = null, ?int $minlength = null, ?string $pattern = null, ?int $pair_attribute = null, ?bool $required = true, ?bool $hidden = false, ?bool $temporary = false, ?bool $multiple = false)
    {
        $this
            ->setAttribute_name($attribute_name)
            ->setSteps($steps)
            ->setValue($value)
            ->setError($error)
            ->setLabel($label)
            ->setMask($mask)
            ->setComment($comment)
            ->setDependency($dependency)
            ->setEnum($enum)
            ->setType($type)
            ->setReadonly($readonly)
            ->setId($id)
            ->setOrderBy($orderBy)
            ->setDateformat($dateformat)
            ->setMaxlength($maxlength)
            ->setMinlength($minlength)
            ->setPattern($pattern)
            ->setPair_attribute($pair_attribute)
            ->setRequired($required)
            ->setHidden($hidden)
            ->setTemporary($temporary)
            ->setMultiple($multiple);
    }
    /**
     * Get attribute_name value
     * @return string
     */
    public function getAttribute_name(): string
    {
        return $this->{'attribute-name'};
    }
    /**
     * Set attribute_name value
     * @param string $attribute_name
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setAttribute_name(string $attribute_name): self
    {
        // validation for constraint: string
        if (!is_null($attribute_name) && !is_string($attribute_name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($attribute_name, true), gettype($attribute_name)), __LINE__);
        }
        $this->attribute_name = $this->{'attribute-name'} = $attribute_name;
        
        return $this;
    }
    /**
     * Get steps value
     * @return string[]
     */
    public function getSteps(): ?array
    {
        return $this->steps;
    }
    /**
     * This method is responsible for validating the values passed to the setSteps method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSteps method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStepsForArrayConstraintsFromSetSteps(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fieldStepsItem) {
            // validation for constraint: enumeration
            if (!\MonetaServiceProviders\EnumType\Step::valueIsValid($fieldStepsItem)) {
                $invalidValues[] = is_object($fieldStepsItem) ? get_class($fieldStepsItem) : sprintf('%s(%s)', gettype($fieldStepsItem), var_export($fieldStepsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Step', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \MonetaServiceProviders\EnumType\Step::getValidValues()));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set steps value
     * @uses \MonetaServiceProviders\EnumType\Step::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Step::getValidValues()
     * @throws InvalidArgumentException
     * @param string[] $steps
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setSteps(?array $steps = null): self
    {
        // validation for constraint: array
        if ('' !== ($stepsArrayErrorMessage = self::validateStepsForArrayConstraintsFromSetSteps($steps))) {
            throw new InvalidArgumentException($stepsArrayErrorMessage, __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($steps) && count($steps) > 2) {
            throw new InvalidArgumentException(sprintf('Invalid count of %s, the number of elements contained by the property must be less than or equal to 2', count($steps)), __LINE__);
        }
        $this->steps = $steps;
        
        return $this;
    }
    /**
     * Add item to steps value
     * @uses \MonetaServiceProviders\EnumType\Step::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\Step::getValidValues()
     * @throws InvalidArgumentException
     * @param string $item
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function addToSteps(string $item): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\Step::valueIsValid($item)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\Step', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \MonetaServiceProviders\EnumType\Step::getValidValues())), __LINE__);
        }
        // validation for constraint: maxOccurs(2)
        if (is_array($this->steps) && count($this->steps) >= 2) {
            throw new InvalidArgumentException(sprintf('You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 2', count($this->steps)), __LINE__);
        }
        $this->steps[] = $item;
        
        return $this;
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setValue(?string $value = null): self
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        
        return $this;
    }
    /**
     * Get error value
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param string $error
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setError(?string $error = null): self
    {
        // validation for constraint: string
        if (!is_null($error) && !is_string($error)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($error, true), gettype($error)), __LINE__);
        }
        $this->error = $error;
        
        return $this;
    }
    /**
     * Get label value
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }
    /**
     * Set label value
     * @param string $label
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setLabel(?string $label = null): self
    {
        // validation for constraint: string
        if (!is_null($label) && !is_string($label)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($label, true), gettype($label)), __LINE__);
        }
        $this->label = $label;
        
        return $this;
    }
    /**
     * Get mask value
     * @return string|null
     */
    public function getMask(): ?string
    {
        return $this->mask;
    }
    /**
     * Set mask value
     * @param string $mask
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setMask(?string $mask = null): self
    {
        // validation for constraint: string
        if (!is_null($mask) && !is_string($mask)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mask, true), gettype($mask)), __LINE__);
        }
        $this->mask = $mask;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get dependency value
     * @return string|null
     */
    public function getDependency(): ?string
    {
        return $this->dependency;
    }
    /**
     * Set dependency value
     * @param string $dependency
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setDependency(?string $dependency = null): self
    {
        // validation for constraint: string
        if (!is_null($dependency) && !is_string($dependency)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dependency, true), gettype($dependency)), __LINE__);
        }
        $this->dependency = $dependency;
        
        return $this;
    }
    /**
     * Get enum value
     * @return \MonetaServiceProviders\StructType\Enum|null
     */
    public function getEnum(): ?\MonetaServiceProviders\StructType\Enum
    {
        return $this->enum;
    }
    /**
     * Set enum value
     * @param \MonetaServiceProviders\StructType\Enum $enum
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setEnum(?\MonetaServiceProviders\StructType\Enum $enum = null): self
    {
        $this->enum = $enum;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \MonetaServiceProviders\EnumType\AttributeType::valueIsValid()
     * @uses \MonetaServiceProviders\EnumType\AttributeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\MonetaServiceProviders\EnumType\AttributeType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \MonetaServiceProviders\EnumType\AttributeType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \MonetaServiceProviders\EnumType\AttributeType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get readonly value
     * @return bool|null
     */
    public function getReadonly(): ?bool
    {
        return $this->readonly;
    }
    /**
     * Set readonly value
     * @param bool $readonly
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setReadonly(?bool $readonly = false): self
    {
        // validation for constraint: boolean
        if (!is_null($readonly) && !is_bool($readonly)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($readonly, true), gettype($readonly)), __LINE__);
        }
        $this->readonly = $readonly;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get orderBy value
     * @return int|null
     */
    public function getOrderBy(): ?int
    {
        return $this->orderBy;
    }
    /**
     * Set orderBy value
     * @param int $orderBy
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setOrderBy(?int $orderBy = null): self
    {
        // validation for constraint: int
        if (!is_null($orderBy) && !(is_int($orderBy) || ctype_digit($orderBy))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderBy, true), gettype($orderBy)), __LINE__);
        }
        $this->orderBy = $orderBy;
        
        return $this;
    }
    /**
     * Get dateformat value
     * @return string|null
     */
    public function getDateformat(): ?string
    {
        return $this->dateformat;
    }
    /**
     * Set dateformat value
     * @param string $dateformat
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setDateformat(?string $dateformat = null): self
    {
        // validation for constraint: string
        if (!is_null($dateformat) && !is_string($dateformat)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateformat, true), gettype($dateformat)), __LINE__);
        }
        $this->dateformat = $dateformat;
        
        return $this;
    }
    /**
     * Get maxlength value
     * @return int|null
     */
    public function getMaxlength(): ?int
    {
        return $this->maxlength;
    }
    /**
     * Set maxlength value
     * @param int $maxlength
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setMaxlength(?int $maxlength = null): self
    {
        // validation for constraint: int
        if (!is_null($maxlength) && !(is_int($maxlength) || ctype_digit($maxlength))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxlength, true), gettype($maxlength)), __LINE__);
        }
        $this->maxlength = $maxlength;
        
        return $this;
    }
    /**
     * Get minlength value
     * @return int|null
     */
    public function getMinlength(): ?int
    {
        return $this->minlength;
    }
    /**
     * Set minlength value
     * @param int $minlength
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setMinlength(?int $minlength = null): self
    {
        // validation for constraint: int
        if (!is_null($minlength) && !(is_int($minlength) || ctype_digit($minlength))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minlength, true), gettype($minlength)), __LINE__);
        }
        $this->minlength = $minlength;
        
        return $this;
    }
    /**
     * Get pattern value
     * @return string|null
     */
    public function getPattern(): ?string
    {
        return $this->pattern;
    }
    /**
     * Set pattern value
     * @param string $pattern
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setPattern(?string $pattern = null): self
    {
        // validation for constraint: string
        if (!is_null($pattern) && !is_string($pattern)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pattern, true), gettype($pattern)), __LINE__);
        }
        $this->pattern = $pattern;
        
        return $this;
    }
    /**
     * Get pair_attribute value
     * @return int|null
     */
    public function getPair_attribute(): ?int
    {
        return $this->{'pair-attribute'};
    }
    /**
     * Set pair_attribute value
     * @param int $pair_attribute
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setPair_attribute(?int $pair_attribute = null): self
    {
        // validation for constraint: int
        if (!is_null($pair_attribute) && !(is_int($pair_attribute) || ctype_digit($pair_attribute))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pair_attribute, true), gettype($pair_attribute)), __LINE__);
        }
        $this->pair_attribute = $this->{'pair-attribute'} = $pair_attribute;
        
        return $this;
    }
    /**
     * Get required value
     * @return bool|null
     */
    public function getRequired(): ?bool
    {
        return $this->required;
    }
    /**
     * Set required value
     * @param bool $required
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setRequired(?bool $required = true): self
    {
        // validation for constraint: boolean
        if (!is_null($required) && !is_bool($required)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($required, true), gettype($required)), __LINE__);
        }
        $this->required = $required;
        
        return $this;
    }
    /**
     * Get hidden value
     * @return bool|null
     */
    public function getHidden(): ?bool
    {
        return $this->hidden;
    }
    /**
     * Set hidden value
     * @param bool $hidden
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setHidden(?bool $hidden = false): self
    {
        // validation for constraint: boolean
        if (!is_null($hidden) && !is_bool($hidden)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($hidden, true), gettype($hidden)), __LINE__);
        }
        $this->hidden = $hidden;
        
        return $this;
    }
    /**
     * Get temporary value
     * @return bool|null
     */
    public function getTemporary(): ?bool
    {
        return $this->temporary;
    }
    /**
     * Set temporary value
     * @param bool $temporary
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setTemporary(?bool $temporary = false): self
    {
        // validation for constraint: boolean
        if (!is_null($temporary) && !is_bool($temporary)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($temporary, true), gettype($temporary)), __LINE__);
        }
        $this->temporary = $temporary;
        
        return $this;
    }
    /**
     * Get multiple value
     * @return bool|null
     */
    public function getMultiple(): ?bool
    {
        return $this->multiple;
    }
    /**
     * Set multiple value
     * @param bool $multiple
     * @return \MonetaServiceProviders\StructType\Field
     */
    public function setMultiple(?bool $multiple = false): self
    {
        // validation for constraint: boolean
        if (!is_null($multiple) && !is_bool($multiple)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($multiple, true), gettype($multiple)), __LINE__);
        }
        $this->multiple = $multiple;
        
        return $this;
    }
}
