<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProviderInfo StructType
 * Meta information extracted from the WSDL
 * - documentation: Краткая информация по провайдеру
 * @subpackage Structs
 */
class ProviderInfo extends AbstractStructBase
{
    /**
     * The providerId
     * @var string|null
     */
    protected ?string $providerId = null;
    /**
     * The targetAccountId
     * @var int|null
     */
    protected ?int $targetAccountId = null;
    /**
     * The subProviderId
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $subProviderId = null;
    /**
     * The name
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The active
     * @var bool|null
     */
    protected ?bool $active = null;
    /**
     * Constructor method for ProviderInfo
     * @uses ProviderInfo::setProviderId()
     * @uses ProviderInfo::setTargetAccountId()
     * @uses ProviderInfo::setSubProviderId()
     * @uses ProviderInfo::setName()
     * @uses ProviderInfo::setComment()
     * @uses ProviderInfo::setActive()
     * @param string $providerId
     * @param int $targetAccountId
     * @param int $subProviderId
     * @param string $name
     * @param string $comment
     * @param bool $active
     */
    public function __construct(?string $providerId = null, ?int $targetAccountId = null, ?int $subProviderId = null, ?string $name = null, ?string $comment = null, ?bool $active = null)
    {
        $this
            ->setProviderId($providerId)
            ->setTargetAccountId($targetAccountId)
            ->setSubProviderId($subProviderId)
            ->setName($name)
            ->setComment($comment)
            ->setActive($active);
    }
    /**
     * Get providerId value
     * @return string|null
     */
    public function getProviderId(): ?string
    {
        return $this->providerId;
    }
    /**
     * Set providerId value
     * @param string $providerId
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setProviderId(?string $providerId = null): self
    {
        // validation for constraint: string
        if (!is_null($providerId) && !is_string($providerId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($providerId, true), gettype($providerId)), __LINE__);
        }
        $this->providerId = $providerId;
        
        return $this;
    }
    /**
     * Get targetAccountId value
     * @return int|null
     */
    public function getTargetAccountId(): ?int
    {
        return $this->targetAccountId;
    }
    /**
     * Set targetAccountId value
     * @param int $targetAccountId
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setTargetAccountId(?int $targetAccountId = null): self
    {
        // validation for constraint: int
        if (!is_null($targetAccountId) && !(is_int($targetAccountId) || ctype_digit($targetAccountId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($targetAccountId, true), gettype($targetAccountId)), __LINE__);
        }
        $this->targetAccountId = $targetAccountId;
        
        return $this;
    }
    /**
     * Get subProviderId value
     * @return int|null
     */
    public function getSubProviderId(): ?int
    {
        return $this->subProviderId;
    }
    /**
     * Set subProviderId value
     * @param int $subProviderId
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setSubProviderId(?int $subProviderId = null): self
    {
        // validation for constraint: int
        if (!is_null($subProviderId) && !(is_int($subProviderId) || ctype_digit($subProviderId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($subProviderId, true), gettype($subProviderId)), __LINE__);
        }
        $this->subProviderId = $subProviderId;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get active value
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }
    /**
     * Set active value
     * @param bool $active
     * @return \MonetaServiceProviders\StructType\ProviderInfo
     */
    public function setActive(?bool $active = null): self
    {
        // validation for constraint: boolean
        if (!is_null($active) && !is_bool($active)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($active, true), gettype($active)), __LINE__);
        }
        $this->active = $active;
        
        return $this;
    }
}
