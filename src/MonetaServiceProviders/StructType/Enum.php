<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for enum StructType
 * @subpackage Structs
 */
class Enum extends AbstractStructBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - choice: item | complexItem
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Item[]
     */
    protected ?array $item = null;
    /**
     * The complexItem
     * Meta information extracted from the WSDL
     * - choice: item | complexItem
     * - choiceMaxOccurs: 1
     * - choiceMinOccurs: 1
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\ComplexItem[]
     */
    protected ?array $complexItem = null;
    /**
     * Constructor method for enum
     * @uses Enum::setItem()
     * @uses Enum::setComplexItem()
     * @param \MonetaServiceProviders\StructType\Item[] $item
     * @param \MonetaServiceProviders\StructType\ComplexItem[] $complexItem
     */
    public function __construct(?array $item = null, ?array $complexItem = null)
    {
        $this
            ->setItem($item)
            ->setComplexItem($complexItem);
    }
    /**
     * Get item value
     * @return \MonetaServiceProviders\StructType\Item[]
     */
    public function getItem(): ?array
    {
        return isset($this->item) ? $this->item : null;
    }
    /**
     * This method is responsible for validating the values passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintsFromSetItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $enumItemItem) {
            // validation for constraint: itemType
            if (!$enumItemItem instanceof \MonetaServiceProviders\StructType\Item) {
                $invalidValues[] = is_object($enumItemItem) ? get_class($enumItemItem) : sprintf('%s(%s)', gettype($enumItemItem), var_export($enumItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \MonetaServiceProviders\StructType\Item, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * This method is responsible for validating the value passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromSetItem($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'complexItem',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property item can\'t be set as the property %s is already set. Only one property must be set among these properties: item, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set item value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Item[] $item
     * @return \MonetaServiceProviders\StructType\Enum
     */
    public function setItem(?array $item = null): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintsFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        // validation for constraint: choice(item, complexItem)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromSetItem($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($item) && count($item) > 1) {
            throw new InvalidArgumentException(sprintf('Invalid count of %s, the number of elements contained by the property must be less than or equal to 1', count($item)), __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * This method is responsible for validating the value passed to the addToItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the addToItem method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromAddToItem($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'complexItem',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property item can\'t be set as the property %s is already set. Only one property must be set among these properties: item, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Add item to item value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Item $item
     * @return \MonetaServiceProviders\StructType\Enum
     */
    public function addToItem(\MonetaServiceProviders\StructType\Item $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\Item) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \MonetaServiceProviders\StructType\Item, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        // validation for constraint: choice(item, complexItem)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromAddToItem($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($this->item) && count($this->item) >= 1) {
            throw new InvalidArgumentException(sprintf('You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 1', count($this->item)), __LINE__);
        }
        $this->item[] = $item;
        
        return $this;
    }
    /**
     * Get complexItem value
     * @return \MonetaServiceProviders\StructType\ComplexItem[]
     */
    public function getComplexItem(): ?array
    {
        return isset($this->complexItem) ? $this->complexItem : null;
    }
    /**
     * This method is responsible for validating the values passed to the setComplexItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setComplexItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateComplexItemForArrayConstraintsFromSetComplexItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $enumComplexItemItem) {
            // validation for constraint: itemType
            if (!$enumComplexItemItem instanceof \MonetaServiceProviders\StructType\ComplexItem) {
                $invalidValues[] = is_object($enumComplexItemItem) ? get_class($enumComplexItemItem) : sprintf('%s(%s)', gettype($enumComplexItemItem), var_export($enumComplexItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The complexItem property can only contain items of type \MonetaServiceProviders\StructType\ComplexItem, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * This method is responsible for validating the value passed to the setComplexItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setComplexItem method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateComplexItemForChoiceConstraintsFromSetComplexItem($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'item',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property complexItem can\'t be set as the property %s is already set. Only one property must be set among these properties: complexItem, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Set complexItem value
     * This property belongs to a choice that allows only one property to exist. It is
     * therefore removable from the request, consequently if the value assigned to this
     * property is null, the property is removed from this object
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\ComplexItem[] $complexItem
     * @return \MonetaServiceProviders\StructType\Enum
     */
    public function setComplexItem(?array $complexItem = null): self
    {
        // validation for constraint: array
        if ('' !== ($complexItemArrayErrorMessage = self::validateComplexItemForArrayConstraintsFromSetComplexItem($complexItem))) {
            throw new InvalidArgumentException($complexItemArrayErrorMessage, __LINE__);
        }
        // validation for constraint: choice(item, complexItem)
        if ('' !== ($complexItemChoiceErrorMessage = self::validateComplexItemForChoiceConstraintsFromSetComplexItem($complexItem))) {
            throw new InvalidArgumentException($complexItemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($complexItem) && count($complexItem) > 1) {
            throw new InvalidArgumentException(sprintf('Invalid count of %s, the number of elements contained by the property must be less than or equal to 1', count($complexItem)), __LINE__);
        }
        if (is_null($complexItem) || (is_array($complexItem) && empty($complexItem))) {
            unset($this->complexItem);
        } else {
            $this->complexItem = $complexItem;
        }
        
        return $this;
    }
    /**
     * This method is responsible for validating the value passed to the addToComplexItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the addToComplexItem method
     * This has to validate that the property which is being set is the only one among the given choices
     * @param mixed $value
     * @return string A non-empty message if the values does not match the validation rules
     */
    public function validateItemForChoiceConstraintsFromAddToComplexItem($value): string
    {
        $message = '';
        if (is_null($value)) {
            return $message;
        }
        $properties = [
            'item',
        ];
        try {
            foreach ($properties as $property) {
                if (isset($this->{$property})) {
                    throw new InvalidArgumentException(sprintf('The property complexItem can\'t be set as the property %s is already set. Only one property must be set among these properties: complexItem, %s.', $property, implode(', ', $properties)), __LINE__);
                }
            }
        } catch (InvalidArgumentException $e) {
            $message = $e->getMessage();
        }
        
        return $message;
    }
    /**
     * Add item to complexItem value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\ComplexItem $item
     * @return \MonetaServiceProviders\StructType\Enum
     */
    public function addToComplexItem(\MonetaServiceProviders\StructType\ComplexItem $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\ComplexItem) {
            throw new InvalidArgumentException(sprintf('The complexItem property can only contain items of type \MonetaServiceProviders\StructType\ComplexItem, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        // validation for constraint: choice(item, complexItem)
        if ('' !== ($itemChoiceErrorMessage = self::validateItemForChoiceConstraintsFromAddToComplexItem($item))) {
            throw new InvalidArgumentException($itemChoiceErrorMessage, __LINE__);
        }
        // validation for constraint: choiceMaxOccurs(1)
        if (is_array($this->complexItem) && count($this->complexItem) >= 1) {
            throw new InvalidArgumentException(sprintf('You can\'t add anymore element to this property that already contains %s elements, the number of elements contained by the property must be less than or equal to 1', count($this->complexItem)), __LINE__);
        }
        $this->complexItem[] = $item;
        
        return $this;
    }
}
