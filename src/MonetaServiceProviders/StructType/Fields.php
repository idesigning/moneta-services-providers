<?php

declare(strict_types=1);

namespace MonetaServiceProviders\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Fields StructType
 * Meta information extracted from the WSDL
 * - documentation: Контейнер для полей - параметров провайдера.
 * @subpackage Structs
 */
class Fields extends AbstractStructBase
{
    /**
     * The field
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \MonetaServiceProviders\StructType\Field[]
     */
    protected ?array $field = null;
    /**
     * Constructor method for Fields
     * @uses Fields::setField()
     * @param \MonetaServiceProviders\StructType\Field[] $field
     */
    public function __construct(?array $field = null)
    {
        $this
            ->setField($field);
    }
    /**
     * Get field value
     * @return \MonetaServiceProviders\StructType\Field[]
     */
    public function getField(): ?array
    {
        return $this->field;
    }
    /**
     * This method is responsible for validating the values passed to the setField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFieldForArrayConstraintsFromSetField(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $fieldsFieldItem) {
            // validation for constraint: itemType
            if (!$fieldsFieldItem instanceof \MonetaServiceProviders\StructType\Field) {
                $invalidValues[] = is_object($fieldsFieldItem) ? get_class($fieldsFieldItem) : sprintf('%s(%s)', gettype($fieldsFieldItem), var_export($fieldsFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The field property can only contain items of type \MonetaServiceProviders\StructType\Field, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set field value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Field[] $field
     * @return \MonetaServiceProviders\StructType\Fields
     */
    public function setField(?array $field = null): self
    {
        // validation for constraint: array
        if ('' !== ($fieldArrayErrorMessage = self::validateFieldForArrayConstraintsFromSetField($field))) {
            throw new InvalidArgumentException($fieldArrayErrorMessage, __LINE__);
        }
        $this->field = $field;
        
        return $this;
    }
    /**
     * Add item to field value
     * @throws InvalidArgumentException
     * @param \MonetaServiceProviders\StructType\Field $item
     * @return \MonetaServiceProviders\StructType\Fields
     */
    public function addToField(\MonetaServiceProviders\StructType\Field $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \MonetaServiceProviders\StructType\Field) {
            throw new InvalidArgumentException(sprintf('The field property can only contain items of type \MonetaServiceProviders\StructType\Field, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->field[] = $item;
        
        return $this;
    }
}
