<?php

declare(strict_types=1);

namespace MonetaServiceProviders;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'Entity' => '\\MonetaServiceProviders\\StructType\\Entity',
            'NameValueAttribute' => '\\MonetaServiceProviders\\StructType\\NameValueAttribute',
            'FieldsInfo' => '\\MonetaServiceProviders\\StructType\\FieldsInfo',
            'Provider' => '\\MonetaServiceProviders\\StructType\\Provider',
            'limit' => '\\MonetaServiceProviders\\StructType\\Limit',
            'attributes' => '\\MonetaServiceProviders\\StructType\\Attributes',
            'Fields' => '\\MonetaServiceProviders\\StructType\\Fields',
            'Fieldsets' => '\\MonetaServiceProviders\\StructType\\Fieldsets',
            'fieldset' => '\\MonetaServiceProviders\\StructType\\Fieldset',
            'Field' => '\\MonetaServiceProviders\\StructType\\Field',
            'enum' => '\\MonetaServiceProviders\\StructType\\Enum',
            'item' => '\\MonetaServiceProviders\\StructType\\Item',
            'complexItem' => '\\MonetaServiceProviders\\StructType\\ComplexItem',
            'GetNextStepRequestType' => '\\MonetaServiceProviders\\StructType\\GetNextStepRequestType',
            'GetNextStepRequest' => '\\MonetaServiceProviders\\StructType\\GetNextStepRequest',
            'GetNextStepResponseType' => '\\MonetaServiceProviders\\StructType\\GetNextStepResponseType',
            'GetNextStepResponse' => '\\MonetaServiceProviders\\StructType\\GetNextStepResponse',
            'GetServiceProvidersRequestType' => '\\MonetaServiceProviders\\StructType\\GetServiceProvidersRequestType',
            'GetServiceProvidersRequest' => '\\MonetaServiceProviders\\StructType\\GetServiceProvidersRequest',
            'GetServiceProvidersResponseType' => '\\MonetaServiceProviders\\StructType\\GetServiceProvidersResponseType',
            'GetServiceProvidersResponse' => '\\MonetaServiceProviders\\StructType\\GetServiceProvidersResponse',
            'FindServiceProviderByIdRequestType' => '\\MonetaServiceProviders\\StructType\\FindServiceProviderByIdRequestType',
            'FindServiceProviderByIdRequest' => '\\MonetaServiceProviders\\StructType\\FindServiceProviderByIdRequest',
            'FindServiceProviderByIdResponseType' => '\\MonetaServiceProviders\\StructType\\FindServiceProviderByIdResponseType',
            'FindServiceProviderByIdResponse' => '\\MonetaServiceProviders\\StructType\\FindServiceProviderByIdResponse',
            'ProviderInfo' => '\\MonetaServiceProviders\\StructType\\ProviderInfo',
            'AsyncRequest' => '\\MonetaServiceProviders\\StructType\\AsyncRequest',
            'AsyncResponse' => '\\MonetaServiceProviders\\StructType\\AsyncResponse',
        ];
    }
}
